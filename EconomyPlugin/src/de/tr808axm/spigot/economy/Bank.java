/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.economy;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Used to manage balances. Use API to access from other classes: EconomyAPI
 * Created by tr808axm on 06.04.2016.
 */
public class Bank {
    private final EconomyPlugin plugin;
    private final Map<UUID, BigDecimal> balances;
    private String currency;

    public Bank(EconomyPlugin plugin) {
        this.plugin = plugin;
        this.balances = new HashMap<>();
        readConfigs();
    }

    private void readConfigs() {
        plugin.getLogger().info("Reading balances from balances.yml...");
        ConfigurationSection balancesConfigurationSection = plugin.getBalancesConfig().getConfigurationSection("balances");
        if(balancesConfigurationSection == null) balancesConfigurationSection = plugin.getBalancesConfig().createSection("balances");
        for(String uuidString : balancesConfigurationSection.getKeys(false)) {
            balances.put(UUID.fromString(uuidString), new BigDecimal(balancesConfigurationSection.getString(uuidString)));
        }
        plugin.getLogger().info("...found " + balances.size() + " balances!");

        currency = plugin.getConfig().getString("currency");
        plugin.getLogger().info("Current currency is: " + currency);
    }

    public void saveConfig() {
        plugin.getLogger().info("Saving balances to balances.yml...");
        ConfigurationSection balancesConfigurationSection = plugin.getBalancesConfig().getConfigurationSection("balances");
        if(balancesConfigurationSection == null) balancesConfigurationSection = plugin.getBalancesConfig().createSection("balances");
        for(UUID uuid : balances.keySet()) {
            balancesConfigurationSection.set(uuid.toString(), balances.get(uuid).toString());
        }
        try {
            plugin.getBalancesConfig().save(plugin.getBalancesConfigFile());
        } catch (IOException e) {
            plugin.getLogger().severe("Error at saving balances.yml: " + e.getClass().getSimpleName());
        }
        plugin.getLogger().info("Successfully saved balances.yml!");
    }

    public boolean transfer(OfflinePlayer from, OfflinePlayer to, BigDecimal amount, boolean notify) {
        return from != null && to != null && transfer(from.getUniqueId(), to.getUniqueId(), amount, notify);
    }

    public boolean transfer(UUID from, UUID to, BigDecimal amount, boolean notify) {
        if(from != null && to != null && !from.equals(to) && amount.compareTo(BigDecimal.ZERO) > 0) {
            if(remove(from, amount, false) && add(to, amount, false)) {
                if(notify) {
                    plugin.getChatUtil().send(from, format(amount) + " were transferred from your balance to " + plugin.getServer().getOfflinePlayer(to).getName() + "'s!", false);
                    plugin.getChatUtil().send(to, format(amount) + " were transferred from " + plugin.getServer().getOfflinePlayer(from).getName() + "'s balance to yours!", true);
                }
                return true;
            }
        }
        return false;
    }

    public boolean add(OfflinePlayer player, BigDecimal amount, boolean notify) {
        return player != null && add(player.getUniqueId(), amount, notify);
    }

    public boolean add(UUID uuid, BigDecimal amount, boolean notify) {
        if(uuid != null && amount.compareTo(BigDecimal.ZERO) > 0) {
            if (set(uuid, getBalance(uuid).add(amount), false)) {
                if(notify) plugin.getChatUtil().send(uuid, "You've been added " + format(amount) + "!", true);
                return true;
            }
        }
        return false;
    }

    public boolean remove(OfflinePlayer player, BigDecimal amount, boolean notify) {
        return player != null && remove(player.getUniqueId(), amount, notify);
    }

    public boolean remove(UUID uuid, BigDecimal amount, boolean notify) {
        if(uuid != null && amount.compareTo(BigDecimal.ZERO) > 0 && getBalance(uuid).compareTo(amount) >= 0) {
            if(set(uuid, getBalance(uuid).subtract(amount), false)) {
                if(notify) plugin.getChatUtil().send(uuid, "You've been removed " + format(amount) + ChatUtil.NEGATIVE + "!", false);
                return true;
            }
        }
        return false;
    }

    public boolean set(OfflinePlayer player, BigDecimal amount, boolean notify) {
        return player != null && set(player.getUniqueId(), amount, notify);
    }

    public boolean set(UUID uuid, BigDecimal amount, boolean notify) {
        if(uuid != null && amount.compareTo(BigDecimal.ZERO) >= 0) {
            balances.put(uuid, amount);
            if(notify) plugin.getChatUtil().send(uuid, "Your balance was set to " + format(amount) + "!", true);
            return true;
        }
        return false;
    }

    public BigDecimal getBalance(OfflinePlayer player) {
        return player != null ? getBalance(player.getUniqueId()) : null;
    }

    public BigDecimal getBalance(UUID uuid) {
        if(uuid != null) {
            BigDecimal balance = balances.get(uuid);
            if(balance == null) balance = new BigDecimal("0.0");
            return balance;
        }
        return null;
    }

    public String getFormattedBalance(OfflinePlayer player) {
        if(player != null) {
            return getFormattedBalance(player.getUniqueId());
        }
        return null;
    }

    public String getFormattedBalance(UUID uuid) {
        if(uuid != null) {
            return format(getBalance(uuid));
        }
        return null;
    }

    public String format(BigDecimal amount) {
        if(amount == null) amount = new BigDecimal("0");
        DecimalFormat df = new DecimalFormat("#0.00#" + currency);
        return ChatUtil.VARIABLE + df.format(amount) + ChatUtil.POSITIVE;
    }
}
/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.economy;

import org.bukkit.OfflinePlayer;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Used to provide an API with reference on Bank.
 * Created by tr808axm on 07.04.2016.
 */
@SuppressWarnings("unused") // As this is an API, methods won't be used this module.
public class EconomyAPI {
    private static Bank bank;

    public EconomyAPI(EconomyPlugin plugin) {
        bank = plugin.getBank();
    }

    public static boolean transfer(OfflinePlayer from, OfflinePlayer to, BigDecimal amount, boolean notify) {
        return bank.transfer(from, to, amount, notify);
    }

    public static boolean transfer(UUID from, UUID to, BigDecimal amount, boolean notify) {
        return bank.transfer(from, to, amount, notify);
    }

    public static boolean add(OfflinePlayer player, BigDecimal amount, boolean notify) {
        return bank.add(player, amount, notify);
    }

    public static boolean add(UUID uuid, BigDecimal amount, boolean notify) {
        return bank.add(uuid, amount, notify);
    }

    public static boolean remove(OfflinePlayer player, BigDecimal amount, boolean notify) {
        return bank.remove(player, amount, notify);
    }

    public static boolean remove(UUID uuid, BigDecimal amount, boolean notify) {
        return bank.remove(uuid, amount, notify);
    }

    public static boolean set(OfflinePlayer player, BigDecimal amount, boolean notify) {
        return bank.set(player, amount, notify);
    }

    public static boolean set(UUID uuid, BigDecimal amount, boolean notify) {
        return bank.set(uuid, amount, notify);
    }

    public static BigDecimal getBalance(OfflinePlayer player) {
        return bank.getBalance(player);
    }

    public static BigDecimal getBalance(UUID uuid) {
        return bank.getBalance(uuid);
    }

    public static String getFormattedBalance(OfflinePlayer player) {
        return bank.getFormattedBalance(player);
    }

    public static String getFormattedBalance(UUID uuid) {
        return bank.getFormattedBalance(uuid);
    }

    public static String format(BigDecimal amount) {
        return bank.format(amount);
    }
}
/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.economy;

import de.tr808axm.spigot.economy.command.EconomyCommandExecutor;
import de.tr808axm.spigot.pluginutils.ChatUtil;
import de.tr808axm.spigot.pluginutils.ProxiedCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;

/**
 * Economy-plugin's Main class.
 * Created by tr808axm on 06.04.2016.
 */
public class EconomyPlugin extends JavaPlugin {
    private ChatUtil chatUtil;
    private Bank bank;
    private File balancesConfigFile;
    private FileConfiguration balancesConfig;

    @Override
    public void onDisable() {
        super.onDisable();
        bank.saveConfig();
        getLogger().info("Disabled!");
    }

    @Override
    public void onEnable() {
        super.onEnable();
        balancesConfigFile = new File(getDataFolder(), "balances.yml");
        if(!createConfigs()) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        balancesConfig = new YamlConfiguration();
        try {
            balancesConfig.load(balancesConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            getLogger().severe("Error at loading balances.yml: " + e.getClass().getSimpleName());
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        chatUtil = new ChatUtil(getDescription(), ChatColor.GREEN, getServer());
        bank = new Bank(this);
        new EconomyAPI(this);
        registerCommands();
        getLogger().info("Enabled!");
    }

    private void registerCommands() {
        getCommand("economy").setExecutor(new EconomyCommandExecutor(this));
        getCommand("balance").setExecutor(new ProxiedCommandExecutor(getCommand("economy"), "balance"));
        getCommand("transfer").setExecutor(new ProxiedCommandExecutor(getCommand("economy"), "transfer"));
    }

    private boolean createConfigs() {
        try {
            if(!getDataFolder().exists()) //noinspection ResultOfMethodCallIgnored
                getDataFolder().mkdirs();
            if(!new File(getDataFolder(), "config.yml").exists()) {
                getLogger().info("config.yml not found, creating!");
                saveDefaultConfig();
            }
            if(!balancesConfigFile.exists()) {
                getLogger().info("balances.yml not found, creating!");
                copy(getResource("balances.yml"), balancesConfigFile);
            }
            return true;
        } catch (IOException e) {
            getLogger().severe("Error at creating balances.yml: " + e.getClass().getSimpleName());
            return false;
        }
    }

    private void copy(InputStream in, File balancesFile) throws IOException {
        OutputStream out = new FileOutputStream(balancesFile);
        byte[] buffer = new byte[1024];
        int length;
        while((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }
        in.close();
        out.close();
    }

    public ChatUtil getChatUtil() {
        return chatUtil;
    }

    public Bank getBank() {
        return bank;
    }

    public FileConfiguration getBalancesConfig() {
        return balancesConfig;
    }

    public File getBalancesConfigFile() {
        return balancesConfigFile;
    }
}
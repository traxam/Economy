/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.economy.command;

import de.tr808axm.spigot.economy.EconomyPlugin;
import de.tr808axm.spigot.pluginutils.ChatUtil;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles the /economy command.
 * Created by tr808axm on 06.04.2016.
 */
public class EconomyCommandExecutor implements CommandExecutor {
    private final EconomyPlugin plugin;
    private final Map<String, CommandHelp> commandHelp;

    private class CommandHelp {
        public final String node;
        public final String baseCommand;
        public final String helpMessage;
        public final String[] commands;

        /**
         * Creates a help entry
         *
         * @param node        The key of this command.
         * @param baseCommand The title of the help menu. (Don't use aliases or parameters)
         * @param helpMessage The help message displayed next to this command.
         * @param commands    The actual command and its aliases.
         */
        public CommandHelp(String node, String baseCommand, String helpMessage, String... commands) {
            this.node = node;
            this.baseCommand = baseCommand;
            this.helpMessage = helpMessage;
            this.commands = commands;
        }

        public String[] build() {
            String[] messages = new String[5 + commands.length];
            messages[0] = "----- " + baseCommand + " | Help menu -----";
            messages[1] = "";
            messages[2] = ChatUtil.VARIABLE + helpMessage;
            messages[3] = "";
            messages[4] = ChatUtil.DETAILS + "Usage:";
            for (int i = 0; i < commands.length; i++) //Print all aliases
                messages[i + 5] = ChatUtil.DETAILS + "  " + commands[i];
            return messages;
        }
    }

    public String[] buildHelpMenu() {
        String[] messages = new String[1 + (3 * commandHelp.size())];
        messages[0] = "----- Commands | Help menu -----";
        int i = 1;
        for (String node : commandHelp.keySet()) {
            CommandHelp help = commandHelp.get(node);
            messages[i++] = "";
            messages[i++] = ChatUtil.VARIABLE + help.commands[0]; // Don't print the aliases
            messages[i++] = ChatUtil.DETAILS + help.helpMessage;
        }
        return messages;
    }

    public EconomyCommandExecutor(EconomyPlugin plugin) {
        this.plugin = plugin;
        CommandHelp[] commandHelps = new CommandHelp[]{
                new CommandHelp("economy.help",
                        "/economy",
                        "Show the economy help menu.",
                        "/economy [help]", "/econ [help]"),
                new CommandHelp("economy.add",
                        "/economy add",
                        "Add <amount> to <player>'s balance.",
                        "/economy add <player> <amount>"),
                new CommandHelp("economy.balance",
                        "/balance",
                        "Show your or <player>'s current balance",
                        "/economy balance [<player>]", "/balace [<player>]", "/bal [<player>]", "/money [<player>]"),
                new CommandHelp("economy.remove",
                        "/economy remove",
                        "Remove <amount> from <player>'s balance.",
                        "/economy remove <player> <amount>", "/economy rem <player> <amount>"),
                new CommandHelp("economy.set",
                        "/economy set",
                        "Set <player>'s balance to <amount>.",
                        "/economy set <player> <amount>"),
                new CommandHelp("economy.transfer",
                        "/transfer",
                        "Transfer <amount> from your to <player>'s balance.",
                        "/economy transfer <player> <amount>", "/economy trans <player> <amount>", "/transfer <player> <amount>", "/trans <player> amount")
        };
        this.commandHelp = new HashMap<>();
        for (CommandHelp help : commandHelps)
            commandHelp.put(help.node, help);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length >= 1) {
            if (args[0].equalsIgnoreCase("add")) {
                if (!checkPermission(sender, true) || !checkParameterCount(sender, args.length, 3, "economy.add"))
                    return true;
                EconomyOperationData operationData = parseOperationData(sender, args[1], args[2]);
                if (operationData == null) return true;

                if (plugin.getBank().add(operationData.destination, operationData.amount, true))
                    plugin.getChatUtil().send(sender, "Successfully added " + plugin.getBank().format(operationData.amount)
                            + " to " + operationData.destination.getName() + "'s balance!", true);
                else
                    plugin.getChatUtil().send(sender, "Could not add " + plugin.getBank().format(operationData.amount)
                            + ChatUtil.NEGATIVE + " to " + operationData.destination.getName() + "'s balance.", false);
                return true;
            } else if (args[0].equalsIgnoreCase("balance") || args[0].equalsIgnoreCase("bal")) { //TODO add console view player
                if (!checkIsPlayer(sender)) return true;
                if (args.length >= 2 && checkPermission(sender, false)) {
                    Player player = parsePlayer(sender, args[1]);
                    if (player == null) return true;
                    plugin.getChatUtil().send(sender, player.getName() + "'s current balance: " + plugin.getBank().getFormattedBalance(player), true);
                    return true;
                }
                plugin.getChatUtil().send(sender, "Your current balance: " + plugin.getBank().getFormattedBalance((Player) sender), true);
                return true;
            } else if (args[0].equalsIgnoreCase("help")) {
                plugin.getChatUtil().send(sender, buildHelpMenu(), true);
                return true;
            } else if (args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("rem")) {
                if (!checkPermission(sender, true) || !checkParameterCount(sender, args.length, 3, "economy.remove"))
                    return true;
                EconomyOperationData operationData = parseOperationData(sender, args[1], args[2]);
                if (operationData == null) return true;

                if (plugin.getBank().remove(operationData.destination, operationData.amount, true))
                    plugin.getChatUtil().send(sender, "Successfully removed " + plugin.getBank().format(operationData.amount)
                            + " from " + operationData.destination.getName() + "'s balance!", true);
                else
                    plugin.getChatUtil().send(sender, "Could not remove " + plugin.getBank().format(operationData.amount)
                            + ChatUtil.NEGATIVE + " from " + operationData.destination.getName() + "'s balance.", false);
                return true;
            } else if (args[0].equalsIgnoreCase("set")) {
                if (!checkPermission(sender, true) || !checkParameterCount(sender, args.length, 3, "economy.set"))
                    return true;
                EconomyOperationData operationData = parseOperationData(sender, args[1], args[2]);
                if (operationData == null) return true;

                if (plugin.getBank().set(operationData.destination, operationData.amount, true))
                    plugin.getChatUtil().send(sender, "Successfully set " + operationData.destination.getName()
                            + "'s balance to " + plugin.getBank().format(operationData.amount) + "!", true);
                else
                    plugin.getChatUtil().send(sender, "Could not set " + operationData.destination.getName()
                            + "'s balance to " + plugin.getBank().format(operationData.amount) + ChatUtil.NEGATIVE + ".", false);
                return true;
            } else if (args[0].equalsIgnoreCase("transfer") || args[0].equalsIgnoreCase("trans")) {
                if (!checkIsPlayer(sender) || !checkParameterCount(sender, args.length, 3, "economy.transfer"))
                    return true;
                EconomyOperationData operationData = parseOperationData(sender, args[1], args[2]);
                if (operationData == null) return true;

                if (plugin.getBank().transfer((Player) sender, operationData.destination, operationData.amount, true))
                    plugin.getChatUtil().send(sender, "Successfully transferred " + plugin.getBank().format(operationData.amount)
                            + " to " + operationData.destination.getName() + "!", true);
                else
                    plugin.getChatUtil().send(sender, "Could not transfer " + plugin.getBank().format(operationData.amount)
                            + ChatUtil.NEGATIVE + " to " + operationData.destination.getName() + ".", false);
                return true;
            }
            plugin.getChatUtil().send(sender, ChatUtil.INVALID_ARGUMENTS_MESSAGE, false);
            return true;
        }
        plugin.getChatUtil().send(sender, buildHelpMenu(), true);
        return true;
    }

    private class EconomyOperationData {
        public final OfflinePlayer destination;
        public final BigDecimal amount;

        EconomyOperationData(OfflinePlayer destination, BigDecimal amount) {
            this.destination = destination;
            this.amount = amount;
        }
    }

    private EconomyOperationData parseOperationData(CommandSender sender, String destinationPlayerName, String amountString) {
        Player player = parsePlayer(sender, destinationPlayerName);
        BigDecimal amount = parseAmount(sender, amountString);
        if (player == null || amount == null) return null;
        return new EconomyOperationData(parsePlayer(sender, destinationPlayerName), parseAmount(sender, amountString));
    }

    private boolean checkParameterCount(CommandSender sender, int argCount, int minParameterCount, String commandHelpNode) {
        if (argCount >= minParameterCount)
            return true;
        plugin.getChatUtil().send(sender, commandHelp.get(commandHelpNode).build(), true);
        return false;
    }

    private boolean checkIsPlayer(CommandSender sender) {
        if (sender instanceof Player)
            return true;
        plugin.getChatUtil().send(sender, ChatUtil.NO_EXECUTION_FROM_CONSOLE, false);
        return false;
    }

    private boolean checkPermission(CommandSender sender, boolean sendNoPermMessage) {
        if (sender.isOp())
            return true;
        if (sendNoPermMessage)
            plugin.getChatUtil().send(sender, ChatUtil.NO_PERMISSION_MESSAGE, false);
        return false;
    }

    private Player parsePlayer(CommandSender sender, String name) {
        Player player = plugin.getServer().getPlayerExact(name);
        if (player != null) return player;
        plugin.getChatUtil().send(sender, "There is no player with the name '" + ChatUtil.VARIABLE + name + ChatUtil.NEGATIVE + "' online!", false);
        return null;
    }

    private BigDecimal parseAmount(CommandSender sender, String amountString) {
        try {
            return new BigDecimal(amountString);
        } catch (NumberFormatException e) {
            plugin.getChatUtil().send(sender, "'" + ChatUtil.VARIABLE + amountString + ChatUtil.NEGATIVE + "' isn't a valid number!", false);
            return null;
        }
    }
}